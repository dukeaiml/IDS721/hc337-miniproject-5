#!/bin/bash

# Define your DynamoDB table name
TABLE_NAME="miniproject-5"
CSV_FILE="quotes.csv"

# Function to add a quote to DynamoDB
add_quote() {
    TEXT=$1
    AUTHOR=$2
    json=$(cat <<-END
    {
        "author": {"S": "${AUTHOR}"},
        "text": {"S": ${TEXT}}
    }
END
)
    json=$(echo "$json" | tr -d '\n' | tr -d '\r' )

    # Use the AWS CLI to add the quote to DynamoDB
    aws dynamodb put-item \
        --table-name $TABLE_NAME \
        --item "${json}"
}
# Check if CSV file exists
if [ ! -f "$CSV_FILE" ]; then
    echo "CSV file not found: $CSV_FILE"
    exit 1
fi

skip_headers=1
# Loop through each line in the CSV file and add it to DynamoDB
while IFS="|" read -r text author; do
    if ((skip_headers))
    then
        ((skip_headers--))
    else
        add_quote "$text" "$author"
        echo "Added quote to DynamoDB: $text - $author"
    fi
done < "$CSV_FILE"

echo "All quotes have been added to DynamoDB."
