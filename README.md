# hc337-miniproject-5 [![Pipeline Status](https://gitlab.com/dukeaiml/IDS721/hc337-miniproject-5/badges/master/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/hc337-miniproject-5/-/pipelines)

This is an AWS Lambda presents a random quote to the user. Quotes are stored in DynamoDB as a (author, text) pair.

## Getting started

Install [Cargo Lambda](https://www.cargo-lambda.info/guide/getting-started.html) and then clone the git repo:

```bash
git clone https://gitlab.com/dukeaiml/IDS721/hc337-miniproject-5.git
```

Then, create an AWS user with permissions to have full access to Lambda and DynamoDB. Add this user to your shell using the AWS CLI.

### Create and Initialize DynamoDB

We will leverage `aws-cli` for creating the database.

Run the command below to create the database. Ensure that your user has adequate permissions to manage DynamoDB.

```
$ ./create_db.sh
```

Add quotes to the database using the following command

```
$ ./add_quotes.sh
```

If you need to delete the database, you can run

```
$ ./delete_db.sh
```

### Running the lambda``

```bash
# deploy the lambda function locally
make watch

# invoke the lambda function locally
make invoke

# deploy the lambda function remotely
make deploy

# invoke the lambda function remotely
make invoke-remote
```

Optionally, you can go to the AWS Lambda dashboard and add an `API Gateway` trigger. This will allow you to navigate to a URL and invoke the lambda, as shown below.

![Website Screenshot](screenshots/website.png)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.
