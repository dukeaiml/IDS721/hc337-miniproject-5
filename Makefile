deploy: build
	cargo lambda deploy hc337-miniproject-5
watch:
	cargo lambda watch
build:
	cargo lambda build --release
invoke:
	cargo lambda invoke miniproject-5 --data-example apigw-request
invoke-remote:
	cargo lambda invoke --remote hc337-miniproject-5 --data-example apigw-request
format:
	cargo fmt --quiet
lint:
	cargo clippy --quiet
test:
	cargo test --quiet
all: format lint test
